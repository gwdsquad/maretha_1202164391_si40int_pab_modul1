package com.example.maretha_1202164391_si40int_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText panjangTinggi;
    private EditText panjangAlas;
    private TextView perhitunganHasil;
    int tinggi;
    int alas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        perhitunganHasil = findViewById(R.id.text_hasil);
        panjangTinggi = findViewById(R.id.text_tinggi);
        panjangAlas = findViewById(R.id.text_alas);
    }

    public void hitungLuas(View view) {
        tinggi = Integer.parseInt(panjangTinggi.getText().toString());
        alas = Integer.parseInt(panjangAlas.getText().toString());
        perhitunganHasil.setText(String.valueOf(tinggi * alas));
    }
}
